package me.benana.std.both;

import java.util.Scanner;

import me.benana.std.discord.BananaCommands;
import me.benana.std.discord.DiscordCore;

public class Core {
	
	/**
	 * The main function. It runs first.
	 * @param args
	 */
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.print("Token > ");
		DiscordCore.token = s.nextLine(); // I have to get here the token because I can publish it on BitBucket 0.0
		System.out.print("Secret Password > ");
		BananaCommands.secret_password = s.nextLine(); // ^^
		DiscordCore.runFirst();
	}
	
}
