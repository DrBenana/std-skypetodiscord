package me.benana.std.discord;

import com.samczsun.skype4j.chat.Chat;
import com.samczsun.skype4j.exceptions.ConnectionException;

import me.benana.std.skype.SkypeCore;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

public class MessageReceieveEvent implements IListener<MessageReceivedEvent>{

	@Override
	public void handle(MessageReceivedEvent e) {		
		try {
			// Checks where is the command from.
			if (e.getChannel().getName().equals("commands")) return;
			SkypeCore core = DiscordCore.skypes.get(DiscordCore.username.get(e.getAuthor().getName())); // Gets the SkypeCore of the user
			Chat c = core.getLastChatByName(e.getChannel().getName()); // Gets the chat on the skype
			if (c == null) {
				DiscordCore.sendMessage(DiscordCore.getChannelByName(e.getGuild(), "commands", true), "The chat is not found.");
				return;
			}
			try {
				c.sendMessage(e.getMessage().getContent()); // Sends the message on Skype
			} catch (ConnectionException e1) {
				DiscordCore.sendMessage(DiscordCore.getChannelByName(e.getGuild(), "commands", true), "There is a problem with the connection to skype.");
			}
		} catch (NullPointerException exp) {
		}
	}	

}
