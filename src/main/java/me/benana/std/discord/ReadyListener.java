package me.benana.std.discord;

import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.ReadyEvent;
import sx.blah.discord.handle.obj.IGuild;

/**
 * Runs when the bot is ready.
 * 
 * @author DrBenana
 */
public class ReadyListener implements IListener<ReadyEvent> {

	@Override
	public void handle(ReadyEvent event) {
		DiscordCore.getClient().changePlayingText("try &help");
		for (IGuild g : event.getClient().getGuilds()) {
			DiscordCore.sendMessage(DiscordCore.getChannelByName(g, "commands", true), "I'm online now! Remember to reconnect. :)");
		}
	}

}
