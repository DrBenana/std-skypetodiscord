package me.benana.std.discord;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

/**
 * Here's the AdminCommands. You have to use the secret password to enable this option.
 * @author DrBenana
 */
public class BananaCommands implements IListener<MessageReceivedEvent> {
	private static List<String> allowed = new ArrayList<>();
	private static List<String> blocked = new ArrayList<>();
	public static String secret_password = "";
	
	@Override
	public void handle(MessageReceivedEvent e) {
		// Checks if you're found in the commands channel + If the message has more than 1 word.
		if (!e.getChannel().getName().equals("commands") || !e.getMessage().getContent().contains(" ")) {
			DiscordCore.getChannelByName(e.getGuild(), "commands", true);
			return;
		}
		String[] args = e.getMessage().getContent().split(" ");
		
		// The command that enables the BananaCommands. You have to use the secret password at the first argument.
		if (args[0].equals("||secret||")) {
			// Checks if you're blocked.
			if (blocked.contains(e.getAuthor().getName() + e.getAuthor().getStringID())) {
				e.getChannel().sendMessage("You are blocked to use this command.");
				return;
			}
			// Checks the secret password.
			if (secret_password.equals(args[1])) {
				allowed.add(e.getAuthor().getName() + e.getAuthor().getStringID());
				e.getChannel().sendMessage("Hey, You have admin permissions now.");
			} else {
				// Blocks you when the password is wrong.
				blocked.add(e.getAuthor().getName() + e.getAuthor().getStringID());
				e.getChannel().sendMessage("Hey, You've blocked from the secret section of STD. You'll get access in one minute.");
				new Timer().schedule(new TimerTask() {
					@Override
					public void run() {
						blocked.remove(e.getAuthor().getName() + e.getAuthor().getStringID());
					}
				}, 1000*60);
			}
			return;
		}
		
		/*if (!allowed.contains(e.getAuthor().getName() + e.getAuthor().getStringID())) {
			e.getChannel().sendMessage("Hey, You aren't an admin.");
			return;
		}*/
		
		// Broadcasts messages
		if (args[0].equalsIgnoreCase("||broadcast||")) {
			if (!allowed.contains(e.getAuthor().getName() + e.getAuthor().getStringID())) {
				e.getChannel().sendMessage("Hey, You aren't an admin.");
				return;
			}
			
			StringBuilder msg = new StringBuilder();
			for (int i = 1; i < args.length; i++) {
				msg.append(args[i] + " ");
			}
			
			DiscordCore.sendBroadcast(msg.toString());
		}
	}
	
}
