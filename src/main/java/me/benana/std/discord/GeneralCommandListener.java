package me.benana.std.discord;

import java.util.Date;

import com.samczsun.skype4j.chat.Chat;

import me.benana.std.skype.SkypeCore;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.EmbedBuilder;

/**
 * Listens to your messages [COMMANDS]
 * 
 * @author DrBenana
 */
public class GeneralCommandListener implements IListener<MessageReceivedEvent> {

	@Override
	public void handle(MessageReceivedEvent e) {
		// Checks if you're found at the commands channel.
		if (!e.getChannel().getName().equals("commands")) {
			DiscordCore.getChannelByName(e.getGuild(), "commands", true);
			return;
		}

		String msg = e.getMessage().getContent().toLowerCase();

		// The login command.
		if (msg.startsWith("&login")) {
			// Look down
			if (!msg.contains(" ")) {
				DiscordCore.sendMessage(e.getChannel(), "Invalid Syntax. Use &login [Username] [Password]");
				return;
			}
			String[] args = e.getMessage().getContent().split(" ");

			if (args.length != 3) {
				DiscordCore.sendMessage(e.getChannel(), "Invalid Syntax. Use &login [Username] [Password]");
				return;
			}
			// Until here, It checks if you have enough arguments.

			IMessage imsg = DiscordCore.sendMessage(e.getChannel(), "Trying to connect. Please wait...");
			SkypeCore skype = new SkypeCore(); // Creates a new SkypeCore to the user
			DiscordCore.guilds.put(args[1], e.getGuild());
			DiscordCore.skypes.put(args[1], skype);
			DiscordCore.username.put(e.getAuthor().getName(), args[1]);
			if (skype.loginToSkype(args[1], args[2])) { // Try to loging in to the skype
				imsg.edit("Connected! Have fun!");
			} else
				imsg.edit("FAILED :(");
		}

		// The logout command.
		if (msg.startsWith("&logout")) {
			DiscordCore.skypes.get(DiscordCore.username.get(e.getAuthor().getName())).logout();
			DiscordCore.skypes.remove(DiscordCore.username.get(e.getAuthor().getName()));
			DiscordCore.guilds.remove(DiscordCore.username.get(e.getAuthor().getName()));
			DiscordCore.username.remove(e.getAuthor().getName());
			DiscordCore.sendMessage(e.getChannel(), "Disconnected from skype. You can login again.");
		}

		// The help command
		if (msg.startsWith("&help")) {
			EmbedBuilder builder = new EmbedBuilder(); // Build the message
			builder.withAuthorName("**DocBanana**");
			builder.withAuthorIcon("https://d30y9cdsu7xlg0.cloudfront.net/png/53209-200.png");
			builder.withTitle("Help");
			builder.withDescription("Here's the help.");
			builder.appendField("Information",
					"Hey! Welcome to **STD** (Skype-To-Discord). You don't have to use skype anymore! \n"
							+ "STD is a bot that sends messages from skype to discord and from discord to skype. Note that the bot doesn't work with calls and friend requests right now.",
					false);
			builder.appendField("Commands", "Here's the commands of the bot\n\n"
					+ "&login <Username> <Password> \n&logout \n&chat <Username> {Creates a chat with a specific user} \n&request <Username> **{DISABLED}**", false);
			builder.withColor(0, 0, 200);
			builder.withTimestamp(new Date().getTime()); // Finish to build
			DiscordCore.sendMessage(e.getChannel(), builder.build()); // Send the message
		}

		// Create chat command
		if (msg.startsWith("&chat")) {
			// Look down
			if (!msg.contains(" ")) {
				DiscordCore.sendMessage(e.getChannel(), "Invalid Syntax. Use &login [Username] [Password]");
				return;
			}
			String[] args = e.getMessage().getContent().split(" ");
			// Until here, It checks if you have enough arguments.
			
			for (int i = 1; i < args.length; i++) {
				SkypeCore core = DiscordCore.skypes.get(DiscordCore.username.get(e.getAuthor().getName()));
				Chat c = core.getContact(args[i]);
				if (c==null) continue;
				core.tryToCreateNewChannel(SkypeCore.getName(c), c.getIdentity());
				core.sendMessageToDiscord("commands", "The chat has been created");
			}
			
		}

		// Doesn't work. I don't really know why, TODO I'll check it later
		if (msg.startsWith("&request")) {
			DiscordCore.sendMessage(e.getChannel(), "The command is disabled for now.");
		}

	}
}
