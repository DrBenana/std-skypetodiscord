package me.benana.std.discord;

import java.util.HashMap;
import java.util.Map;

import me.benana.std.skype.SkypeCore;
import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.api.internal.json.objects.EmbedObject;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.MessageBuilder;

/**
 * The core of the bot.
 * 
 * @author DrBenana
 */
public class DiscordCore {
	private static IDiscordClient client;
	public static Map<String, IGuild> guilds = new HashMap<>();
	public static Map<String, SkypeCore> skypes = new HashMap<>();
	public static Map<String, String> username = new HashMap<>();
	public static String token = "";
	
	/**
	 * Enables the bot.
	 */
	public static void runFirst() {
		loginToDiscord();
		registerEvents(new GeneralCommandListener());
		registerEvents(new ReadyListener());
		registerEvents(new MessageReceieveEvent());
		registerEvents(new BananaCommands());
	}
	
	/**
	 * @return IDiscordClient of the current bot.
	 */
	public static IDiscordClient loginToDiscord() {
		ClientBuilder builder = new ClientBuilder();
		builder.withToken(token);
		client = builder.login();
		return getClient();
	}
	
	/**
	 * @return IDiscordClient of the current bot.
	 */
	public static IDiscordClient getClient() {
		return client;
	}
	
	/**
	 * Register events to the bot.
	 * @param e
	 */
	public static void registerEvents(IListener<?> e) {
		getClient().getDispatcher().registerListener(e);
	}
	
	/**
	 * Sends a string message to some channel.
	 * @param channel The channel to send to.
	 * @param message The message to send.
	 * @return IMessage that sent.
	 */
	public static IMessage sendMessage(IChannel channel, String message) {
		return new MessageBuilder(getClient()).withChannel(channel).withContent(message).build();
	}
	
	/**
	 * Sends a string message to some channel.
	 * @param channel The channel to send to.
	 * @param message The message to send.
	 * @return IMessage that sent.
	 */
	public static IMessage sendMessage(IChannel channel, EmbedObject message) {
		return channel.sendMessage(message);
	} 
	
	/**
	 * Get a channel from a specific guild by its name.
	 * @param guild The guild to get the channel from.
	 * @param name The name of the channel.
	 * @param id The id of the channel (If it has any)
	 * @param create Creates the channel if it doesn't exist.
	 * @return The channel.
	 */
	public static IChannel getChannelByName(IGuild guild, String name, String id, boolean create) {
		// Moves on the channels at the specific guild.
		for (IChannel c : guild.getChannels()) {
			// Checks if something is null.
			if (c == null || c.getPinnedMessages() == null) {
				sendMessage(getChannelByName(guild, "commands", true), "Hey, Please don't create channels yourself. It cause bugs.");
				continue;
			}
			// Checks the pinned messages on the channel. (The ID is saved via pinned message)
			for (IMessage msg : c.getPinnedMessages()) {
				if (msg.getContent().startsWith("ID: ") && msg.getContent().replaceFirst("ID: ", "").equals(id)) {
					return c;
				}
			}
		}
		
		// Creates the channel if true.
		if (create) {
			IChannel newChannel = guild.createChannel(name);
			newChannel.pin(sendMessage(newChannel, "ID: " + id));
			return newChannel;
		}
		else 
			return null;
		
	}
	
	/**
	 * Get a channel from a specific guild by its name (Without an ID)
	 * @param guild The guild to get the channel from.
	 * @param name The name of the channel.
	 * @param create Creates the channel if it doesn't exist.
	 * @return The channel.
	 */
	public static IChannel getChannelByName(IGuild guild, String name, boolean create) {		
		for (IChannel c : guild.getChannels())
			if (c.getName().equalsIgnoreCase(name)) return c;
		
		if (create) {
			IChannel newChannel = guild.createChannel(name);
			return newChannel;
		}
		else 
			return null;
		
	}
	
	/**
	 * Broadcasts message to all of the guilds.
	 * @param msg The message to broadcast.
	 */
	public static void sendBroadcast(String msg) {
		getClient().getGuilds().forEach(g -> sendMessage(getChannelByName(g, "announcements", true), msg));
	}
	
}
