package me.benana.std.skype;

import com.samczsun.skype4j.chat.GroupChat;
import com.samczsun.skype4j.events.EventHandler;
import com.samczsun.skype4j.events.Listener;
import com.samczsun.skype4j.events.chat.message.MessageReceivedEvent;
import com.samczsun.skype4j.exceptions.ConnectionException;

import sx.blah.discord.handle.obj.IChannel;

/**
 * Runs when you receive a message.
 * 
 * @author DrBenana
 */
public class ReceiveMessageEvent implements Listener {
	private SkypeCore core;
	
	public ReceiveMessageEvent(SkypeCore core) {
		this.core = core;
	}
	
	@EventHandler
	public void onReceiveMessage(MessageReceivedEvent e) {		
		try {
			// Tries to send the message to the discord.
			IChannel c = core.tryToCreateNewChannel(SkypeCore.getName(e.getChat()), e.getChat().getIdentity());
			c.sendMessage("**" + e.getMessage().getSender().getDisplayName() + ":** " + e.getMessage().getContent().asPlaintext());
			if (e.getChat() instanceof GroupChat) c.changeTopic(((GroupChat) e.getChat()).getTopic());
		} catch (ConnectionException e1) {
			core.sendMessageToDiscord("commands", "Hey, I've got a connection problem from skype so I can't send you messages.");
		}
	}
	
}
