package me.benana.std.skype;

import com.samczsun.skype4j.chat.GroupChat;
import com.samczsun.skype4j.events.EventHandler;
import com.samczsun.skype4j.events.Listener;
import com.samczsun.skype4j.events.chat.message.MessageSentEvent;

import sx.blah.discord.handle.obj.IChannel;

/**
 * Runs when you sends message on skype.
 * 
 * @author DrBenana
 */
public class SentMessageEvent implements Listener {
	private SkypeCore core;
	
	public SentMessageEvent(SkypeCore core) {
		this.core = core;
	}
	
	@EventHandler
	public void onSentMessage(MessageSentEvent e) {
		// Creates the channel.
		IChannel c = core.tryToCreateNewChannel(SkypeCore.getName(e.getChat()), e.getChat().getIdentity());
		if (e.getChat() instanceof GroupChat) c.changeTopic(((GroupChat) e.getChat()).getTopic());

	}
	
}
