package me.benana.std.skype;

import com.samczsun.skype4j.Skype;
import com.samczsun.skype4j.SkypeBuilder;
import com.samczsun.skype4j.chat.Chat;
import com.samczsun.skype4j.chat.GroupChat;
import com.samczsun.skype4j.events.Listener;
import com.samczsun.skype4j.exceptions.ChatNotFoundException;
import com.samczsun.skype4j.exceptions.ConnectionException;
import com.samczsun.skype4j.exceptions.InvalidCredentialsException;
import com.samczsun.skype4j.exceptions.NoSuchContactException;
import com.samczsun.skype4j.exceptions.NotParticipatingException;

import me.benana.std.discord.DiscordCore;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;

/**
 * The core of the SkypeBots. 
 * 
 * @author DrBenana
 */
public class SkypeCore {
	private Skype skype;
	private String username;
	
	/**
	 * Loging in to the SkypeBot
	 * @param username The username of the user
	 * @param password The password of the user
	 * @return Successful
	 */
	public boolean loginToSkype(String username, String password) {
		this.username = username;
		skype = new SkypeBuilder(username, password).withAllResources().build();
		try {
			skype.login();
			Listener[] listeners = {new ReceiveMessageEvent(this), new SentMessageEvent(this)}; // The listeners
			for (Listener l : listeners) skype.getEventDispatcher().registerListener(l); // Register the listeners
			skype.subscribe();
			System.out.println(username + " uses STD! :D"); // Debug
			return true;
		} catch (NotParticipatingException e) {
			sendMessageToDiscord("commands", "[Skype] The user doesn't exist.");
			return false;
		} catch (InvalidCredentialsException e) {
			sendMessageToDiscord("commands", "[Skype] Something is wrong.");
			return false;
		} catch (ConnectionException e) {
			sendMessageToDiscord("commands", "[Skype] The connection has been lost.");
			return false;
		}
	}
	
	/**
	 * Logouts from SkypeBot
	 */
	public void logout() {
		try {
			skype.logout();
		} catch (ConnectionException e) {
			sendMessageToDiscord("commands", "[Skype] The connection has been lost.");
		}
	}
	
	/**
	 * Tries to create a channel if it doesn't exist.
	 * @param channel The name of the channel
	 * @param id The id of the chat
	 * @return The channel
	 */
	public IChannel tryToCreateNewChannel(String channel, String id) {
		return DiscordCore.getChannelByName(DiscordCore.guilds.get(skype.getUsername()), channel, id, true);
	}
	
	/**
	 * Tries to create a channel if it doesn't exist.
	 * @param channel The name of the channel
	 * @return The channel
	 */
	public IChannel tryToCreateNewChannel(String channel) {
		return DiscordCore.getChannelByName(DiscordCore.guilds.get(skype.getUsername()), channel, true);
	}
	
	/**
	 * Sends a message to the Discord.
	 * @param channel The name of the channel.
	 * @param msg The message to send.
	 * @param id The ID of the chat.
	 */
	public void sendMessageToDiscord(String channel, String msg, String id) {
		DiscordCore.sendMessage(tryToCreateNewChannel(channel, id), msg);
	}
	
	/**
	 * Sends a message to the Discord.
	 * @param channel The name of the channel.
	 * @param msg The message to send.
	 */
	public void sendMessageToDiscord(String channel, String msg) {
		DiscordCore.sendMessage(tryToCreateNewChannel(channel), msg);
	}
	
	/**
	 * @return Username on skype.
	 */
	public String getUsername() {
		return username;
	}
	
	/**
	 * Tries to get the last chat with the specific name on skype.
	 * @param name The name of the chat.
	 * @return The last chat with the name.
	 */
	public Chat getLastChatByName(String name) {
		for (IChannel c : DiscordCore.guilds.get(getUsername()).getChannels()) { // Tries the channels
			if (!c.getName().equalsIgnoreCase(name)) continue; // Checks the name
			for (IMessage msg : c.getPinnedMessages()) { // Checks the pinned messages
				if (msg.getContent().startsWith("ID: ")) {
					try {
						return skype.getOrLoadChat(msg.getContent().replaceFirst("ID: ", "")); // Successful
					} catch (ConnectionException e) {
						sendMessageToDiscord("commands", "Hey, I've got a connection problem from skype. The chat is not going to work well.");
					} catch (ChatNotFoundException e) {
						sendMessageToDiscord("commands", "Hey, The current chat is removed from skype.");
					}
				}
			}
		}
		return null;
	}
	
	/**
	 * Get a contact by a nmae
	 * @param name The username of the contact
	 * @return Chat of the contact
	 */
	public Chat getContact(String name) {
		try {
			return skype.getOrLoadChat("8:" + name);
		} catch (ConnectionException e) {
			sendMessageToDiscord("commands", "There's a connection problem with skype. Please try later.");
		} catch (ChatNotFoundException e) {
			sendMessageToDiscord("commands", "The contact does not exist.");
		}
		return null;
	}
	
	/**
	 * Friends request. Doesn't seem to work.
	 * @param name The username of the user to send the request to.
	 * @param msg The message to send him.
	 */
	public void request(String name, String msg) {
		try {
			skype.getOrLoadContact("8:" + name).sendRequest(msg);
			sendMessageToDiscord("commands", "The friend request has been sent.");
		} catch (ConnectionException e) {
			sendMessageToDiscord("commands", "There's a connection problem with skype. Please try later.");
		} catch (NoSuchContactException e) {
			sendMessageToDiscord("commands", "The contact does not exist.");
		}
	}
	
	/**
	 * Gets a name of specific chat via the limits of discord.
	 * @param c
	 * @return
	 */
	public static String getName(Chat c) {
		String name = "";
		if (c instanceof GroupChat) {
			GroupChat chat = (GroupChat) c;
			name = chat.getTopic(); // If it's a group, It's not problem.
		} else {
			name = c.getIdentity().replaceFirst(c.getIdentity().split(":")[0], ""); // If it's an user, We need the identify.
		}
		name = name.replaceAll("[^A-Za-z]+", "_"); // Discord doesn't support special characters.
		while (name.startsWith("_")) { // Removes the '_' at the start. The name in the discord can't start with '_'
			name = name.replaceFirst("_", "");
		}
		
		if (name.length() < 2) { // The name has to be at least 2 characters.
			name = c.getIdentity().replaceFirst(c.getIdentity().split(":")[0], ""); // Tries the identify. It usually works.
			name = name.replaceAll("[^A-Za-z]+", "_");
			while (name.startsWith("_")) {
				name = name.replaceFirst("_", "");
			}
		}
		return name; // Returns the name, Easy.
	}
}
